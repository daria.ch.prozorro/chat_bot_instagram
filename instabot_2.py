from selenium import webdriver
import time
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from list_users_insta import queryBox
from config import username, password
from sms_insta import textbox


service = Service(exsecutive_path='/usr/local/bin/chromedriver')
driver = webdriver.Chrome(service=service)


try:
    driver.get('https://www.instagram.com/direct/inbox/')
    time.sleep(5)

    user = driver.find_element(by=By.NAME, value='username')
    user.clear()
    user.send_keys(username)
    time.sleep(5)
    user = driver.find_element(by=By.NAME, value='password')
    user.clear()
    user.send_keys(password)
    time.sleep(5)
    user.send_keys(Keys.ENTER)
    time.sleep(10)

    driver.execute_script('document.getElementsByClassName("_acan _acap _acas _aj1-")[0].click()')
    time.sleep(10)
    driver.execute_script('document.getElementsByClassName("_a9-- _a9_0")[0].click()')
    time.sleep(10)

    driver.execute_script('document.getElementsByClassName("x1i10hfl xjqpnuy xa49m3k xqeqjp1 x2hbi6w x972fbf xcfux6l '
                          'x1qhh985 xm0m39n xdl72j9 x2lah0s xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r x2lwn1j xeuugli '
                          'xexx8yu x18d9i69 x1hl2dhg xggy1nq x1ja2u2z x1t137rt x1q0g3np x1lku1pv x1a2a7pz x6s0dn4 xjyslct '
                          'x1lq5wgf xgqcy7u x30kzoy x9jhf4c x1ejq31n xd10rxx x1sy0etr x17r0tee x9f619 x9bdzbf x1ypdohk '
                          'x78zum5 x1i0vuye x1f6kntn xwhw2v2 x10w6t97 xl56j7k x17ydfre x1swvt13 x1pi30zi x1n2onr6 x2b8uid '
                          'xlyipyv x87ps6o x14atkfc xcdnw81 x1tu34mt xzloghq")[0].click()')
    time.sleep(10)

    send_user = driver.find_element(by=By.NAME, value='queryBox')
    send_user.send_keys(queryBox)
    time.sleep(5)
    driver.execute_script('document.getElementsByClassName("x9f619 xjbqb8w x78zum5 x168nmei x13lgxp2 x5pf9jr xo71vjh x1uhb9sk '
                          'x1plvlek xryxfnj x1iyjqo2 x2lwn1j xeuugli xdt5ytf xqjyukv x1cy8zhl x1oa3qoh x1nhvcw1")[0].click()')
    time.sleep(10)
    driver.execute_script('document.getElementsByClassName("x1i10hfl xjqpnuy xa49m3k xqeqjp1 x2hbi6w x972fbf xcfux6l '
                          'x1qhh985 xm0m39n xdl72j9 x2lah0s xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r x2lwn1j xeuugli '
                          'xexx8yu x18d9i69 x1hl2dhg xggy1nq x1ja2u2z x1t137rt x1q0g3np x1lku1pv x1a2a7pz x6s0dn4 xjyslct '
                          'x1lq5wgf xgqcy7u x30kzoy x9jhf4c x1ejq31n xd10rxx x1sy0etr x17r0tee x9f619 x9bdzbf x1ypdohk '
                          'x78zum5 x1i0vuye x1f6kntn xwhw2v2 xl56j7k x17ydfre x1n2onr6 x2b8uid xlyipyv x87ps6o x14atkfc '
                          'xcdnw81 xn3w4p2 x5ib6vp xc73u3c x1tu34mt xzloghq")[0].click()')
    time.sleep(10)

    sms = driver.find_element(by=By.XPATH, value='/html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/'
                                                 'section/div/div/div/div[1]/div/div[2]/div/div/div/div/div/div[2]/'
                                                 'div/div/div[2]/div/div/div[2]/div/div[1]/p')
    sms.clear()
    sms.send_keys(textbox)
    time.sleep(5)
    sms.send_keys(Keys.ENTER)
    time.sleep(15)


except Exception as ex:
    print(ex)



